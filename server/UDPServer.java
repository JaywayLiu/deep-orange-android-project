import java.net.*;
import java.util.HashSet;
 
public class UDPServer
{	
   //Hash sets to store users IP adresses and port numbers
   private static HashSet<InetAddress> IPSet = new HashSet<InetAddress>();
   private static HashSet<Integer> portSet = new HashSet<Integer>();
 
   public static void main(String args[]) throws Exception
   {
 
	   //Server always listens on port 5560    
      int serverPort = 5560;        
 
	   //Open new socket on 5560 to accept connections
	   DatagramSocket serverSock = new DatagramSocket(serverPort);        
 
	   System.out.println("Server started...\n");
 
	   while(true)
		{
		   //Buffer to hold message data
			byte[] rcvData = new byte[1024];          
 
			//Packet to hold message data
			DatagramPacket rcvPacket = new DatagramPacket(rcvData, rcvData.length);
 
			//Receieve any incoming packets
			serverSock.receive(rcvPacket);           
 
			//Extract message data
			String rcvMessage = (new String(rcvPacket.getData()));
 
			//Log connection and message
			System.out.println("Client Connected on " + rcvPacket.getSocketAddress());
			System.out.println("Client message: \"" + rcvMessage + "\"");          
 
			//Get IP of client 
			InetAddress clientIP = rcvPacket.getAddress();           
 
			//Print out IP and hostname of client
			System.out.println("Client IP & Hostname: " + clientIP + ", " + clientIP.getHostName() + "\n");
 
			//Get port number
			int clientPort = rcvPacket.getPort();
         //Add client IP and Port to Hashsets
			IPSet.add(clientIP);
         portSet.add(clientPort);
          
         //Log message to return to client 
			System.out.println(rcvMessage);
			//Buffer to send data back
			byte[] sendData  = new byte[1024];
 
			// Assign the message to the send buffer
			sendData = rcvMessage.getBytes();
			
         //Sends received message to all connections except origin conneciton
			for(InetAddress ip : IPSet) 
			{
			   if(ip != clientIP) 
				{
               for(int p : portSet)
               {
                  if(p != clientPort)
                  {
					      //Datagram to send message to specified connection
     					   DatagramPacket relayMessage = new DatagramPacket(sendData, sendData.length, ip, p); 
			   		   // Send the relayed message          
				   	   serverSock.send(relayMessage);
                  } 
               }   
			   }
	      }
      }
   }
}
