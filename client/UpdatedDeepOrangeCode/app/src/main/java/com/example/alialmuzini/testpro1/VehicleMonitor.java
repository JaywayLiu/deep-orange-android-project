package com.example.alialmuzini.testpro1;

public class VehicleMonitor {
	private static String message;
	private static String hardware_id;
	private static String name;
	
	public String getMessage() {
		return message;
	}
	
	public String getID() {
		return hardware_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setMessage(String mes) {
		message = mes;
	}
	
	public void setID(String id) {
		hardware_id = id;
	}
	
	public void setName(String nam) {
		name = nam;
	}
	
	public VehicleMonitor() {
	}
}
