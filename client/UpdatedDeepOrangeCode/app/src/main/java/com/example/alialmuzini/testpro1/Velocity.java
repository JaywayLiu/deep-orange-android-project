package com.example.alialmuzini.testpro1;

public class Velocity {
	private static int linearX;
	private static int linearY;
	private static int linearZ;
	
	private static int angularX;
	private static int angularY;
	private static int angularZ;
	
	public int getlinearX() {
		return linearX;
	}
	
	public int getlinearY() {
		return linearY;
	}
	
	public int getlinearZ() {
		return linearZ;
	}
	
	public int getangularX() {
		return angularX;
	}
	
	public int getangularY() {
		return angularY;
	}
	
	public int getangularZ() {
		return angularZ;
	}
	
	public void setlinearX(int x) {
		linearX = x;
	}
	
	public void setlinearY(int y) {
		linearY = y;
	}
	
	public void setlinearZ(int z) {
		linearZ = z;
	}
	
	public void setangularX(int x) {
		angularX = x;
	}
	
	public void setangularY(int y) {
		angularY = y;
	}
	
	public void setangularZ(int z) {
		angularZ = z;
	}
	
	public Velocity() {
	}

}
