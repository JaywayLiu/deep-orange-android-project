package com.example.alialmuzini.testpro1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by alialmuzini on 3/30/17.
 */

public class MyView extends View {

    public MyView(Context context){
        super(context);
        init();

    }
    private void init()
    {
        frameN = 0;
        realx = 12f;
        realy = 4.6f;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.TRANSPARENT);
        //canvas.drawPaint(paint);
        //paint.setColor(Color.parseColor("#da4747"));
        paint.setColor(Color.parseColor("#00B5FF"));
        list = new ArrayList<PointF>();

        for (int i=0; i<80; i++) {
            list.add(new PointF(realx+i, realy));
        }
    }

    public MyView(Context context, AttributeSet at){
        super(context, at);
        init();
    }

    public MyView(Context context, AttributeSet at, int defStyle){
        super(context, at, defStyle);
        init();
    }
    public void draw(float x, float y)
    {
        realx = x;
        realy = y;
        invalidate();


    }
    private void drawOnce() {
        //super.onDraw(canvas);
        float w = getWidth();
        float h = getHeight();

        System.out.println("h value = " + h);
        System.out.println("w value = " + w);

        int viewW = getLayoutParams().width;
        int viewH = getLayoutParams().height;

        int radius = 35;

        PointF re;
        re = convertToImageSpace(w, h, realx, realy);
        ca.drawCircle(re.x, re.y, radius, paint);

        System.out.println("frameN ="+frameN+" realx="+realx+" realy="+realy);
    }

    @Override
    protected void onDraw(Canvas canvas){
        ca = canvas;
        super.onDraw(canvas);
        drawOnce();
    }

    // function to convert the realx and realy to the image space on canvas
    public PointF convertToImageSpace(float w, float h, float realx, float realy)
    {
        PointF re = new PointF(0,0);
        re.x =  realx / (float)167.055 * w;
        re.y = (1 - (realy/80.0f + 0.305f/2.007f)) *h;

        System.out.println("re.x = "+ re.x);
        System.out.println("re.y = " + re.y);

        return re;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        System.out.println("called when click");
            draw(list.get(frameN).x, list.get(frameN).y);
        frameN +=1;
        if(frameN==list.size())
            frameN = 0;

        return super.onTouchEvent(event);
    }

    @Override
    public boolean callOnClick(){

     System.out.println("called when click");
        for(int i=0; i<list.size(); i++) {
            draw(list.get(frameN).x, list.get(frameN).y);
        }
        frameN +=1;
        return true;

    }

    public Runnable m_handlerTask;
    public Handler  m_handler;
    private Canvas ca;
    int frameN;
    private float realx;
    private float realy;
    private Paint paint;
    private ArrayList<PointF> list;

}
