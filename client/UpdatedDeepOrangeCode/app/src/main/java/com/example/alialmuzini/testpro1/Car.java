package com.example.alialmuzini.testpro1;

public class Car {
	private static Acceleration acc;
	private static String vehicle_ID;
	private static String behavior;
	private static Location gps;
	private static Battery charge;
	private static Velocity speed;
	private static VehicleMonitor monitor;
	
	public static Acceleration getAcc() {
		return acc;
	}
	
	public static String getID() {
		return vehicle_ID;
	}
	
	public static String getBehavior() {
		return behavior;
	}
	
	public static Location getGPS() {
		return gps;
	}
	
	public static Battery getCharge() {
		return charge;
	}
	
	public static Velocity getSpeed() {
		return speed;
	}
	
	public static VehicleMonitor getMonitor() {
		return monitor;
	}
	
	public static void setAcc(Acceleration accel) {
		acc = accel;
	}
	
	public static void setID(String id) {
		vehicle_ID = id;
	}
	
	public static void setBehavior(String behav) {
		behavior = behav;
	}
	
	public static void setGPS(Location loc) {
		gps = loc;
	}
	
	public static void setCharge(Battery batt) {
		charge = batt;
	}
	
	public static void setSpeed(Velocity mph) {
		speed = mph;
	}
	
	public static void setMonitor(VehicleMonitor mon) {
		monitor = mon;
	}
	
	private static void Car() {
		acc = new Acceleration();
		//vehicle_ID = "-1";
		//behavior = "None";
		gps = new Location();
		charge = new Battery();
		speed = new Velocity();
		monitor = new VehicleMonitor();
	}
}
