package com.example.alialmuzini.testpro1;

import java.util.Vector;

public class Parser {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getCar(String json) {
		Vector token = new Vector(1);
		Vector tokens = new Vector(1);
		char currentChar = '0';
		
		for(int x = 0; x < json.length(); x++) {
			currentChar = json.charAt(x);
			
			//strings enclosed by " are considered one token
			if(currentChar == 34) {
				x++;
				while(json.charAt(x) != 34){
					token.add(json.charAt(x));		
					x++;
				}
				x++;
				currentChar = json.charAt(x);
			}
			
			//removes special characters { } , : [ ] 
			if(currentChar == 123 || currentChar == 125 || currentChar == 44 || currentChar == 58 || currentChar == 32 || currentChar == 91 || currentChar == 93) {
			}
			else
			{
				token.add(currentChar);
			}
			
			//creates a token when the parser hits a space or EOF
			if(currentChar == 32 || x == json.length() - 1) {
				char[] chars = new char[token.size()];
				for(int y = 0; y < token.size(); y++) {
					chars[y] = (char) token.get(y);
				}
				tokens.add(new String(chars));
				token.removeAllElements();
				
			}
			
		} 
		
		//parse created tokens and add them to a static car class
		for(int x = 0; x < tokens.size(); x++) {
			//gets the vehicle's acceleration from the tokens
			if(((String) tokens.get(x)).equals("acceleration")) {
				Acceleration acc = Car.getAcc();
				while(!((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){				
					x++;
					if(((String) tokens.get(x)).equals("linear")) {
						while(!((String) tokens.get(x)).equals("angular") && !((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
							x++; 
							if(((String) tokens.get(x)).equals("x")) {
								x++;
								acc.setlinearX(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("y")) {
								x++;
								acc.setlinearY(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("z")) {
								x++;
								acc.setlinearZ(Integer.parseInt(((String) tokens.get(x))));
							}
							
						}
					}

					if(((String) tokens.get(x)).equals("angular")) {
						while(!((String) tokens.get(x)).equals("linear") && !((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
							x++; 
							if(((String) tokens.get(x)).equals("x")) {
								x++;
								acc.setangularX(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("y")) {
								x++;
								acc.setangularY(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("z")) {
								x++;
								acc.setangularZ(Integer.parseInt(((String) tokens.get(x))));
							}
						}
							
					}

				}
			}
			
			//gets the vehicle's id from the tokens
			if(((String) tokens.get(x)).equals("vehicle_id")) {
				x++;
				Car.setID(((String) tokens.get(x)));
			}
			
			//gets the vehicle's behavior from the tokens
			if(((String) tokens.get(x)).equals("behavior")) {
				x++;
				Car.setBehavior(((String) tokens.get(x)));
			}
			
			//gets the vehicle's location from the tokens
			if(((String) tokens.get(x)).equals("GPS_location")) {

				while(!((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
					x++;
					if(((String) tokens.get(x)).equals("altitude")) {
						x++;
						Car.getGPS().setAltitude(Float.parseFloat((String) tokens.get(x)));
					}
					if(((String) tokens.get(x)).equals("latitude")) {
						x++;
						Car.getGPS().setLatitude(Float.parseFloat(((String) tokens.get(x))));
					}
					if(((String) tokens.get(x)).equals("longitude")) {
						x++;
						Car.getGPS().setLongitude(Float.parseFloat(((String) tokens.get(x))));
					}
				}
			}
			
			//gets the vehicle's batter status from the tokens
			if(((String) tokens.get(x)).equals("battery_status")) {
				Battery bat = Car.getCharge();
				while(!((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
					x++;
					if(((String) tokens.get(x)).equals("capacity")) {
						x++;
						bat.setCapacity(Double.parseDouble((String) tokens.get(x)));
					}
					if(((String) tokens.get(x)).equals("charge")) {
						x++;
						bat.setCharge(Double.parseDouble(((String) tokens.get(x))));
					}
					if(((String) tokens.get(x)).equals("percentage")) {
						x++;
						bat.setPercent(Double.parseDouble(((String) tokens.get(x))));
					}
				}
			}
			
			//gets the vehicle's velocity from the tokens
			if(((String) tokens.get(x)).equals("velocity")) {
				Velocity vel = Car.getSpeed();
				while(!((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
					x++;
					if(((String) tokens.get(x)).equals("linear")) {
						while(!((String) tokens.get(x)).equals("angular") && !((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
							x++; 
							if(((String) tokens.get(x)).equals("x")) {
								x++;
								vel.setlinearX(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("y")) {
								x++;
								vel.setlinearY(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("z")) {
								x++;
								vel.setlinearZ(Integer.parseInt(((String) tokens.get(x))));
							}
							
						}
					}

					if(((String) tokens.get(x)).equals("angular")) {
						while(!((String) tokens.get(x)).equals("linear") && !((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && !((String) tokens.get(x)).equals("vehicleMonitor") && x < (tokens.size() - 1)){
							x++; 
							if(((String) tokens.get(x)).equals("x")) {
								x++;
								vel.setangularX(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("y")) {
								x++;
								vel.setangularY(Integer.parseInt(((String) tokens.get(x))));
							}
							if(((String) tokens.get(x)).equals("z")) {
								x++;
								vel.setangularZ(Integer.parseInt(((String) tokens.get(x))));
							}
						}
							
					}
				}
			}
			
			//gets warning messages from the tokens
			if(((String) tokens.get(x)).equals("vehicleMonitor")) {
				VehicleMonitor mon = Car.getMonitor();
				while(!((String) tokens.get(x)).equals("acceleration") && !((String) tokens.get(x)).equals("vehicle_id") && !((String) tokens.get(x)).equals("behavior") && !((String) tokens.get(x)).equals("GPS_location") && !((String) tokens.get(x)).equals("battery_status") && !((String) tokens.get(x)).equals("velocity") && x < (tokens.size() - 1)){
					x++;
					if(((String) tokens.get(x)).equals("message")) {
						x++;
						mon.setMessage((String) tokens.get(x));
					}
					if(((String) tokens.get(x)).equals("hardware_id")) {
						x++;
						mon.setID((String) tokens.get(x));
					}
					if(((String) tokens.get(x)).equals("name")) {
						x++;
						mon.setName((String) tokens.get(x));
					}
				}
			}
		}
	}

}