package com.example.alialmuzini.testpro1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import java.io.*;
import java.net.*;

public class SecondActivity extends AppCompatActivity {
    MyView cus;
    Sender s;
    Receiver r;

    public void sendStop(View view)
    {
        s.send("STOP");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);
        RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(400, 200);

        cus = (MyView) findViewById(R.id.myView);
        assert (cus != null);
        int clientPort = 5560;
        // Get the IP address of the server
        InetAddress addrs = null;
        try {
            addrs = InetAddress.getByName("158.85.106.206");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        //Create a thread to send messages
        s = null;
        try {
            s = new Sender(addrs, clientPort);
        } catch (SocketException e) {
            System.out.println("\n Sender failed to initialize");
        }
        Thread sendThread = new Thread(s, "Thread S");
        //Create a thread to receive messages
        r = null;
        try {
            r = new Receiver(s.getSocket());
        } catch (SocketException e) {
            System.out.println("\n Receiver failed to initialize");
        }
        Thread receiveThread = new Thread(r, "Thread R");
        sendThread.start();
        receiveThread.start();
        System.out.println("\nBoth threads running");

    }

    public void update(float x, float y) {
        cus.draw(x, y);
    }

    public class Sender implements Runnable
    {
        //Varibles for Server's IP Address, the UDP socket, a flag to tell us whether the socket is active or not, and the server port
        private InetAddress serverIP;
        private DatagramSocket clientSocket;
        private boolean inactive = false;
        private int serverPort;
        private String message = "";

        //Constructor creating new socket with a certain target IP address and port
        public Sender(InetAddress addrs, int sp) throws SocketException
        {
            this.serverIP = addrs;
            this.serverPort = sp;
            // Create a UDP socket for the client
            System.out.println("\nTrying to make socket");
            this.clientSocket = new DatagramSocket();
            //Connect to the server at serverIP on serverPort
            System.out.println("\nMade socket");
        }

        //Makes this thread stop by setting the flag to true
        public void halt() {this.inactive = true;}

        //Returns this socket
        public DatagramSocket getSocket() {return this.clientSocket;}

        //
        public void send(String messageData)
        {
            this.message = messageData;
            inactive = false;
        }

        //Execute when thread is run
        public void run()
        {
            try
            {
                this.clientSocket.connect(serverIP, serverPort);
                //Send a blank packet to register client with server
                byte[] blank = new byte[1024];
                blank = "".getBytes();
                DatagramPacket blankPacket = new DatagramPacket(blank,blank.length , serverIP, serverPort);
                clientSocket.send(blankPacket);

                while (true)
                {
                    if (!inactive)
                    {
                        //Create buffer to hold message
                        byte[] sendData = new byte[1024];

                        //Puts message into buffer
                        sendData = this.message.getBytes();

                        //Create a packet to send
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverIP, serverPort);

                        //Send the UDP packet to server
                        System.out.println("\nSent: "+ this.message);
                        clientSocket.send(sendPacket);
                        inactive = true;
                    }
                    Thread.yield();
                }

            }

            catch (IOException ex) { System.err.println("\nTrouble parsing message to send\n"); }
            finally{clientSocket.close();}
        }
    }

    public class Receiver extends Thread {

        private DatagramSocket clientSocket;
        private boolean inactive;

        //Constructor specifying port to listen on
        public Receiver(DatagramSocket ds) throws SocketException {
            this.clientSocket = ds;
            this.inactive = false;
        }

        //Makes the thread stop by setting flag to true
        public void halt() {
            this.inactive = true;
        }

        //When the receiver gets a message, updates the ui based on the changes
        private void publishUpdate( float ex, float why){
            final float x = ex;
            final float y = why;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    update(x,y);
                }
            });
        }

        //Executes when thread is run
        public void run() {
            System.out.println("\nReceiveThread running!");
            byte[] receiveData;
            DatagramPacket receivePacket;
            Parser p = new Parser();

            while (true) {
                System.out.println("\nRunning");
                //Stops if thread is inactive
                if (inactive) return;

                //Buffer to hold message data received
                receiveData = new byte[1024];

                //Packet to receive data
                receivePacket = new DatagramPacket(receiveData, receiveData.length);

                try {
                    //Get packet from server
                    clientSocket.receive(receivePacket);
                    //Set packet data to a string
                    String serverReply = new String(receivePacket.getData(), 0, receivePacket.getLength());
                    p.getCar(serverReply);

                    //Print to screen
                    System.out.println("\nUDPClient: Response from Server: \"" + serverReply + "\"\n");
                       publishUpdate(Car.getGPS().getLatitude(), Car.getGPS().getLongitude());

                    Thread.yield();

                } catch (IOException ex) {
                    System.err.println("\nTrouble parsing received message\n");
                }
            }
        }
    }
}