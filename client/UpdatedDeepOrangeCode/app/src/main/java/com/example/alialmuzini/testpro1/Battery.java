package com.example.alialmuzini.testpro1;

public class Battery {
	private static double capacity;
	private static double charge;
	private static double percentage;
	
	public double getCapacity() {
		return capacity;
	}
	
	public double getCharge() {
		return charge;
	}
	
	public double getPercent() {
		return percentage;
	}
	
	public void setCapacity(double cap) {
		capacity = cap;
	}
	
	public void setCharge(double cha) {
		charge = cha;
	}
	
	public void setPercent(double per) {
		percentage = per;
	}
	
	public Battery() {
	}

}
