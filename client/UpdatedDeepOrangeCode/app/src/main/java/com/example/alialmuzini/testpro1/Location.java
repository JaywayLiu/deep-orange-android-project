package com.example.alialmuzini.testpro1;

public class Location {
	private static float latitude = 0.0f;
	private static float longitude= 0.0f;
	private static float altitude=0.0f;
	
	public static float getLatitude() {
		return latitude;
	}
	
	public static float getLongitude() {
		return longitude;
	}
	
	public static float getAltitude() {
		return altitude;
	}
	
	public static void setLatitude(float lat) {
		latitude = lat;
	}
	
	public static void setLongitude(float lon) {
		longitude = lon;
	}
	
	public static void setAltitude(float alt) {
		altitude = alt;
	}
	
	private static void Location() {
	}
}
